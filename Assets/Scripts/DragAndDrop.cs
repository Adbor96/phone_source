﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDrop : MonoBehaviour
{
    bool moveAllowed;
    Collider2D col;
    private Rigidbody2D rb;

    [SerializeField]
    private float speed;

    //timer som låter en hoppa om man trycker på spelaren snabbt två gånger
    private float jumpTimer;
    private float jumpTimerMax = 1;
    //bool som säger om spelaren kan hoppa
    private bool JumpPossible;

    private bool canJump;
    //bool som känner av om spelaren måste tappas igen
    private bool canIncreaseTap = true;

    //hur många gånger spelaen har tryckt på spelaren
    private int tapAmount;

    [SerializeField]
    private float jumpForce;


    private Animator anim;
    private SpriteRenderer spriteRend;
    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<Collider2D>();
        jumpTimer = 0;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        spriteRend = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //om man rör skärmen
        if (Input.touchCount > 0)
        {
            //beskriver status för fingret som rör skärmen
            Touch touch = Input.GetTouch(0);
            //vart skärmen rördes
            Vector2 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);



            //om skärmen har rörts
            if (touch.phase == TouchPhase.Began)
            {
                Collider2D touchedCollider = Physics2D.OverlapPoint(touchPosition);
                //kollar om det man rörde var spelaren
                if (col == touchedCollider)
                {
                    moveAllowed = true;
                    canIncreaseTap = true;
                }
            }
            //om man drar längs skärmen
            if (touch.phase == TouchPhase.Moved)
            {
                //om du för röra spelaren
                if (moveAllowed)
                {
                    //dras spelaren längs fingret
                    rb.transform.localPosition = new Vector2(touchPosition.x, transform.position.y) * speed;
                    //rb.AddForce(new Vector2(touchPosition.x, rb.transform.position.y));
                    anim.SetBool("isRunning", true);
                    Debug.Log(touch.deltaPosition);
                    if(touch.deltaPosition.x < 0)
                    {
                        spriteRend.transform.localScale = new Vector3(-8, 8, 8);
                    }
                    else
                    {
                        spriteRend.transform.localScale = new Vector3(8, 8, 8);
                    }
                }
            }
            //om man slutar röra skärmen
            if (touch.phase == TouchPhase.Ended)
            {
                anim.SetBool("isRunning", false);
                Debug.Log("Ended");
                //öka tapcount
                if (canIncreaseTap)
                {
                    tapAmount++;
                    canIncreaseTap = false;
                }
                moveAllowed = false;
                canIncreaseTap = true;
            }
        }
        //hopp

        //om man rört spelaren en gång startar timern för att kunna hoppa
        if (tapAmount == 1)
        {
            jumpTimer += jumpTimerMax * Time.deltaTime;
            //om spelaren inte hoppar tillräckligt snabbt ställs timern om
            if (jumpTimer >= jumpTimerMax)
            {
                jumpTimer = 0;
                tapAmount = 0;
            }
        }
        //om man rör spelaren två gånger snabbt
        if (tapAmount == 2)
        {
            Debug.Log("Jump");
            canJump = true;
            jumpTimer = 0;
            tapAmount = 0;
        }
    }
    private void FixedUpdate()
    {
        //om spelaren kan hoppa
        if (canJump)
        {
            //spelaren hoppar
            rb.AddForce(transform.up * jumpForce);
            canJump = false;
        }
    }
}
