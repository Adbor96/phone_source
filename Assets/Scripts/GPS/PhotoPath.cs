﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoPath : MonoBehaviour
{
    [SerializeField] private Text text;
     float imageLatitude = 57.4363f;
     float imageLongitude = 15.1972f;

    //gör om koordinaterna till meter
    float longScale = 60772.16f;
    float latScale = 111359.83f;

    float imageX;
    float imageY;
    // Start is called before the first frame update
    void Start()
    {
        imageX = imageLongitude * longScale;
        imageY = imageLatitude * latScale;
    }

    // Update is called once per frame
    void Update()
    {
        //om gps är påslaget
        if (Input.location.isEnabledByUser)
        {
            if (Input.touchCount > 0 && Input.location.status == LocationServiceStatus.Stopped)
            {
                Input.location.Start(5);
            }
            if(Input.location.status == LocationServiceStatus.Running)
            {
                text.text = Input.location.lastData.latitude + "\n" + Input.location.lastData.longitude;

                Debug.Log("ImageX = " + imageX + "\n ImageY = " + imageY);

                Vector2 imagePosition = new Vector2(imageX, imageY);
                Vector2 phonePosition = new Vector2(Input.location.lastData.longitude * longScale, Input.location.lastData.latitude * latScale);

                if (Vector2.Distance(imagePosition, phonePosition) <= 15)
                {
                    if (Input.touchCount > 0)
                    {
                        text.text = "Correct! \n " + Vector2.Distance(imagePosition, phonePosition).ToString();
                        Debug.Log("You are correct");
                        Debug.Log(Vector2.Distance(imagePosition, phonePosition));
                    }
                }
                else if(Vector2.Distance(imagePosition, phonePosition) > 15)
                {
                    if (Input.touchCount > 0)
                    {
                        text.text = "Incorrect! \n " + Vector2.Distance(imagePosition, phonePosition).ToString();
                        Debug.Log("You are incorrect");
                        Debug.Log(Vector2.Distance(imagePosition, phonePosition));
                    }
                }

            }
            else
            {
                text.text = Input.location.status.ToString();
            }
        }
        else
        {
            text.text = "Touch to start";
        }
    }
}
