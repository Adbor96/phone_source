﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class World : MonoBehaviour
{
    [SerializeField] private List<WorldGpsPositions> positions;
    [SerializeField] private GameObject markerPrefab;
    [SerializeField] private GameObject gpsMarker;
    [SerializeField] private List<GameObject> markers;
    [SerializeField] private float rotateSpeed;
    private Collider2D col;

    private bool canMove;

    private bool markerAdded;
    public Canvas canvas;
    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<Collider2D>();

        markers = new List<GameObject>();

        //för varje position man har märkt ut på globen
        for (int i = 0; i < positions.Count; i++)
        {
            //skapa en "marker" prefab
            GameObject marker = Instantiate(markerPrefab, transform.position, Quaternion.identity);

            marker.transform.Rotate(new Vector3(positions[i].position.x, -positions[i].position.y, 0));
           
            //flytta ut markören från centrum av globen
            marker.transform.Translate(0, 0, -20);

            //gör globen till parent
            marker.transform.parent = transform;

            //ändra markörens färg
            marker.GetComponentInChildren<MeshRenderer>().material.color = positions[i].color;

            //lägg till markören i listan
            markers.Add(marker);

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.location.isEnabledByUser)
        {
            if (Input.location.status == LocationServiceStatus.Stopped)
            {
                Input.location.Start();
            }

            if(Input.location.status == LocationServiceStatus.Running)
            {
                if (!markerAdded)
                {
                    //skapa en ny vektor av telefonens position
                    Vector2 phonePosition = new Vector2(Input.location.lastData.latitude, Input.location.lastData.longitude);

                    //skapa en "marker" prefab
                    GameObject marker = Instantiate(gpsMarker, transform.position, Quaternion.identity);


                    //gör globen till parent
                    marker.transform.parent = transform;

                    marker.transform.rotation = Quaternion.identity;

                    //rotera markören till mobilens position
                    marker.transform.Rotate(new Vector3(phonePosition.x, -phonePosition.y, 0));

                    //flytta ut markören från centrum av globen
                    marker.transform.Translate(0, 0, -20);

                    Debug.Log(Haversine(phonePosition.x, phonePosition.y, positions[0].position.x, positions[0].position.y));

                    marker.GetComponentInChildren<Text>().text = "Your position";

                    for (int i = 0; i < positions.Count; i++)
                    {
                        positions[i].distanceFromMarker = Haversine(phonePosition.x, phonePosition.y, positions[i].position.x, positions[i].position.y);
                       /* Text text = markers[i].AddComponent<Text>();
                        text.text = positions[i].distanceFromMarker.ToString();
                        text.fontSize = 30;*/

                        markers[i].GetComponentInChildren<Text>().text = positions[i].location + "\n" + positions[i].distanceFromMarker.ToString("00") + "km";
                    }
                    markerAdded = true;
                }
            }
        }
        //om man rör skärmen
        if (Input.touchCount > 0)
        {
            //beskriver status för fingret som rör skärmen
            Touch touch = Input.GetTouch(0);
            //vart skärmen rördes
            Vector2 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);



            //om skärmen har rörts
            if (touch.phase == TouchPhase.Began)
            {
                    canMove = true;
            }
            //om man drar längs skärmen
            if (touch.phase == TouchPhase.Moved)
            {
                //om du för röra spelaren
                if (canMove)
                {
                    Debug.Log("Can move");
                    //dras spelaren längs fingret
                    transform.Rotate(0, -touch.deltaPosition.x * rotateSpeed, 0);
                    Debug.Log(touchPosition);
                    //rb.AddForce(new Vector2(touchPosition.x, rb.transform.position.y));
                }
            }
            //om man slutar röra skärmen
            if (touch.phase == TouchPhase.Ended)
            {
                Debug.Log("Ended");
                canMove = false;
            }
        }
    }
    float Haversine(float currentLatitude, float currentLongitude, float latitudeToCompare, float longitudeToCompare)
    {
        //Radius of the earth differ, I'm taking the average. 
        //Haversine formula 
        //distance = R * 2 * aTan2 ( square root of A, square root of 1 - A ) 
        // where A = sinus squared (difference in latitude / 2) + (cosine of latitude 1 * cosine of latitude 2 * sinus squared (difference in longitude / 2)) 
        // and R = the circumference of the earth 

        float resultDistance = 0.0f;
        float avgRadiusOfEarth = 6371.392896f;

        float differenceInLat = Mathf.Deg2Rad * (currentLatitude - latitudeToCompare);

        float differenceInLong = Mathf.Deg2Rad * (currentLongitude - longitudeToCompare);

        float aInnerFormula = Mathf.Cos(Mathf.Deg2Rad *(currentLatitude)) * Mathf.Cos(Mathf.Deg2Rad * (latitudeToCompare)) * Mathf.Sin(differenceInLong / 2) * Mathf.Sin(differenceInLong / 2);

        float aFormula = (Mathf.Sin((differenceInLat) / 2) * Mathf.Sin((differenceInLat) / 2)) + (aInnerFormula);

        resultDistance = avgRadiusOfEarth * 2 * Mathf.Atan2(Mathf.Sqrt(aFormula), Mathf.Sqrt(1 - aFormula));

        return resultDistance;


        //double aInnerFormula = Math.Cos(DegreeToRadian(currentLatitude)) * Math.Cos(DegreeToRadian(latitudeToCompare)) * Math.Sin(differenceInLong / 2) * Math.Sin(differenceInLong / 2);

        //double aFormula = (Math.Sin((differenceInLat) / 2) * Math.Sin((differenceInLat) / 2)) + (aInnerFormula);

    }
}
