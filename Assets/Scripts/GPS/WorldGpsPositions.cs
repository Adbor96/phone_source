﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WorldGpsPositions 
{
    public string location;
    public Color color;
    public Vector2 position;
    public float distanceFromMarker;


    public WorldGpsPositions(string _location, Color _color, Vector2 _position)
    {
        _location = location;
        _color = color;
        _position = position;
    }
}
