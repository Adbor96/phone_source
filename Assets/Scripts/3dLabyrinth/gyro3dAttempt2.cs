﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gyro3dAttempt2 : MonoBehaviour
{
    private bool gyroEnabled;
    private Gyroscope gyro;

    private GameObject boardContainer;
    private Quaternion rotation;
    private Rigidbody rb;

    private void Start()
    {
        //skapar ett nytt gameobject fäst vid spelbrädet
        boardContainer = new GameObject("Board Container");
        boardContainer.transform.position = transform.position;
        transform.SetParent(boardContainer.transform);
        //boardContainer.transform.rotation = transform.rotation;
        rb = GetComponent<Rigidbody>();
        gyroEnabled = enableGyro();
    }
    private void Update()
    {

    }

    private bool enableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            Debug.Log("Supports gyro");

            //det gyroscope vi har assignat i början är mobilens standard gyro
            gyro = Input.gyro;
            //sätt igång gyron
            gyro.enabled = true;

            //boardContainer.transform.rotation = Quaternion.Euler(90, 0, 0);
            rotation = new Quaternion(0, 1, 0, 0);
            return true;
        }
        Debug.LogError("Gyro not supported");
        return false;
    }
    private void FixedUpdate()
    {
        if (gyroEnabled)
        {
            Vector3 phoneRotate = new Vector3(gyro.attitude.x, gyro.attitude.y, gyro.attitude.z);
            phoneRotate /= 2 * Mathf.PI;
            phoneRotate *= 360;
            //quaternions måste vara föra vectors om man multiplicerar de med varandra
            Vector3 gyroInput = gyro.rotationRateUnbiased;
            rb.angularVelocity = new Vector3(-gyroInput.x, -gyroInput.y, -gyroInput.z);
            //rb.rotation = Quaternion.Euler(phoneRotate);
            Debug.Log(gyro.attitude);
        }
    }
}
