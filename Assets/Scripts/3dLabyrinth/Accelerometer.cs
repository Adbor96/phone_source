﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerometer : MonoBehaviour
{
    private Rigidbody rb;
    private bool isFlat = true;
    private Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        Vector3 tilt = Input.acceleration;

        if (isFlat)
        {
            //kompenserar inputten för hur vinklat telefonen är (t.ex om den ligger ner eller man håller den framför sig)
            tilt = Quaternion.Euler(90, 0, 0) * tilt;
        }

        rb.AddForce(tilt);
        //mainCamera.transform.Rotate(new Vector3(Input.acceleration.x, Input.acceleration.y, Input.acceleration.z));
    }
}
