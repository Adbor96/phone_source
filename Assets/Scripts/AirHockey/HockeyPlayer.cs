﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HockeyPlayer : MonoBehaviour
{
    private bool canMove;

    [SerializeField]
    private float speed;

    private Rigidbody2D rb;
    private Collider2D col;

    //hur långt spelaren kan max röra sig på banan
    private float[] maxRange;

    [SerializeField] private float maxDistanceY;
    [SerializeField] private float minDistanceY;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //om spelaren går utanför maxavståndet
        if (rb.transform.position.y >= maxDistanceY)
        {
            rb.transform.position = new Vector2(rb.transform.position.x, maxDistanceY);
        }
        if (rb.transform.position.y <= minDistanceY)
        {
            rb.transform.position = new Vector2(rb.transform.position.x, minDistanceY);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //om spelaren krockar med pucken
        if(collision.gameObject.name == "Puck")
        {

        }
    }
    private void FixedUpdate()
    {
        //om man rör skärmen
        if (Input.touchCount > 0)
        {
            //beskriver status för fingret som rör skärmen
            Touch touch = Input.GetTouch(0);
            //vart skärmen rördes
            Vector2 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);



            //om skärmen har rörts
            if (touch.phase == TouchPhase.Began)
            {
                //Debug.Log("Began");
                Collider2D touchedCollider = Physics2D.OverlapPoint(touchPosition);
                //kollar om det man rörde var spelaren
                if (col == touchedCollider)
                {
                    canMove = true;
                    Debug.Log("Player found");
                }
            }
            //om man drar längs skärmen
            if (touch.phase == TouchPhase.Moved)
            {
                //Debug.Log("Moved");
                //om du kan röra spelaren
                if (canMove)
                {
                    //dras spelaren längs fingret
                    //rb.velocity = touchPosition;
                    rb.MovePosition(touchPosition);
                    //rb.AddForce(new Vector2(touchPosition.x, rb.transform.position.y) * speed);
                }
            }
            //om man slutar röra skärmen
            if (touch.phase == TouchPhase.Ended)
            {
                //Debug.Log("Ended");
                canMove = false;
            }
        }
    }
}
