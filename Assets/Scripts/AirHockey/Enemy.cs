﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float radius;
    private GameObject puck;
    [SerializeField] private LayerMask puckLayer;
    [SerializeField] private float speed;
    private Rigidbody2D rb;
    [SerializeField] private float minY;
    [SerializeField] private float maxY;
    private bool PuckSpotted;

    private Vector2 defaultPosition;
    // Start is called before the first frame update
    void Start()
    {
        puck = GameObject.Find("Puck");
        rb = GetComponent<Rigidbody2D>();
        defaultPosition = rb.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();

        if(rb.transform.position.y >= maxY)
        {
            rb.transform.position = new Vector2(rb.transform.position.x, maxY);
        }
        if(rb.transform.position.y <= minY)
        {
            rb.transform.position = new Vector2(rb.transform.position.x, minY);
        }

    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
    void Shoot()
    {
        //hitta pucken inom avstånd
        Collider2D[] puckCollider = Physics2D.OverlapCircleAll(transform.position, radius, puckLayer);

        foreach(Collider2D puck in puckCollider)
        {
            Debug.Log("Puck found");
            Vector3 direction = puck.transform.position - rb.transform.position;
            direction.Normalize();
            rb.transform.position += direction * speed;
            PuckSpotted = true;
        }
        if(puckCollider.Length == 0)
        {
            PuckSpotted = false;
        }
    }
}
