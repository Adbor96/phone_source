﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPointer : MonoBehaviour
{
    private bool attacking;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //om man rör skärmen
        if (Input.touchCount > 0)
        {
            //beskriver status för fingret som rör skärmen
            Touch touch = Input.GetTouch(0);
            //vart skärmen rördes
            Vector2 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);


            //om skärmen har rörts
            if (touch.phase == TouchPhase.Began)
            {
                //kollar om det man rörde var där man kan attackera

                

            }
            //om man drar längs skärmen
            if (touch.phase == TouchPhase.Moved)
            {
                //Debug.Log(Vector2.Distance(touch.deltaPosition, transform.position));
                if(Vector2.Distance(touch.deltaPosition, transform.position) > 5)
                {
                    attacking = true;
                }
                else
                {
                    attacking = false;
                }
            }
            //om man slutar röra skärmen
            if (touch.phase == TouchPhase.Ended)
            {
                Debug.Log("Ended");
                //öka tapcount

            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        SamuraiEnemy enemy = collision.gameObject.GetComponent<SamuraiEnemy>();
        if (enemy != null)
        {
            Debug.Log("damage enemy");
            enemy.Damage(1);
        }
    }
}

