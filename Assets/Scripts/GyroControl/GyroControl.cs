﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroControl : MonoBehaviour
{

    private GameObject objectContainer;
    private Quaternion rotation;
    private Rigidbody2D rb;

    private void Start()
    {
        //objectContainer = new GameObject("Object Container");
        //objectContainer.transform.position = transform.position;
        //transform.SetParent(objectContainer.transform);
        rb = GetComponent<Rigidbody2D>();
        rotation = rb.transform.rotation;
    }
    

    private void Update()
    {
        //det värde man ändrar med gyrocontrol, är en quaternion
        //gyro.attitude


    }
    private void FixedUpdate()
    {
        //om systemet stöder gyro
        if (SystemInfo.supportsGyroscope)
        {
            Debug.Log("Supports gyro");
            //rb.angularVelocity = Input.gyro.rotationRateUnbiased.z * 100;
            /*Vector3 rotationAsVector = rb.transform.rotation.eulerAngles;
            rotationAsVector.x = 0;
            rotationAsVector.y = 0;
            rb.transform.rotation = Quaternion.Euler(rotationAsVector);*/
            Physics2D.gravity = (Input.gyro.gravity);
        }
        else
        {
            Debug.LogError("Doesnt support gyro");
        }
    }
}
