﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneCamera : MonoBehaviour
{
    //om mobilen har en kamera
    private bool camIsAvailable;

    private WebCamTexture backCamTexture;
    private Texture defaultBackground;

    [SerializeField] private RawImage background;
    [SerializeField] private AspectRatioFitter fit;
    // Start is called before the first frame update
    void Start()
    {
        defaultBackground = background.texture;
        //leta efter kamera
        WebCamDevice[] devices = WebCamTexture.devices;

        //om det finns 0 kameror
        if(devices.Length == 0)
        {
            Debug.LogError("No camera detected!");
            camIsAvailable = false;
            return;
        }
        //gå igenom de kameror som finns
        for (int i = 0; i < devices.Length; i++)
        {
            //om kameran inte pekar framåt
            if (!devices[i].isFrontFacing)
            {
                backCamTexture = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
        }
        //om bak kamera inte hittas
        if (backCamTexture == null)
        {
            Debug.LogError("Unable to find back camera!");
            return;
        }
        //starta kameran
        backCamTexture.Play();
        background.texture = backCamTexture;

        camIsAvailable = true;
    }

    // Update is called once per frame
    void Update()
    {
        //om kameran inte hittas
        if (!camIsAvailable)
            return;

        float ratio = (float)backCamTexture.width / (float)backCamTexture.height;
        fit.aspectRatio = ratio;

        //om kameran är vinkad vertikalt är Y skalan -1, annars är den 1
        float scaleY = backCamTexture.videoVerticallyMirrored ? -1f: 1f;
        background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

        //kamerans vinkel
        int orient = -backCamTexture.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
    }
}
