﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantPlane : MonoBehaviour
{
    [SerializeField] private WebCamTexture camera;
    [SerializeField] private GameObject plane;
    private Texture2D touchTexture;
    // Start is called before the first frame update
    private IEnumerator Start()
    {
        //be om att få använda mobilens kamera
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        plane = GameObject.Find("Plane2");
        camera = new WebCamTexture();

        //ändra objektets renderer till det kameran visar
        plane.GetComponent<Renderer>().material.mainTexture = camera;
        camera.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
