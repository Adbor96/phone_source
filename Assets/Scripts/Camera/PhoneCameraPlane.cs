﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneCameraPlane : MonoBehaviour
{
    [SerializeField] private WebCamTexture camera;
    [SerializeField] private GameObject plane;
    [SerializeField] private GameObject plane2;
    private Texture2D touchTexture;
    // Start is called before the first frame update
    private IEnumerator Start()
    {
        //be om att få använda mobilens kamera
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        plane = GameObject.Find("Plane");
        plane2 = GameObject.Find("Plane2");
        camera = new WebCamTexture();

        //ändra objektets renderer till det kameran visar
        plane.GetComponent<Renderer>().material.mainTexture = camera;
        plane2.GetComponent<Renderer>().material.mainTexture = camera;
        camera.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);



            if (touch.phase == TouchPhase.Ended)
            {
                touchTexture = new Texture2D(camera.width, camera.height, TextureFormat.RGBA32, false);

                touchTexture.SetPixels(camera.GetPixels());

                plane.GetComponent<Renderer>().material.mainTexture = touchTexture;

                touchTexture.Apply(true);
            }
        }
    }
}
