﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneSwordAttack : MonoBehaviour
{
    private Collider2D attackCollider;
    [SerializeField] private GameObject attackIcon;
    private bool canAttack;
    [SerializeField] private float attackSpeed;
    // Start is called before the first frame update
    void Start()
    {
        attackCollider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //om man rör skärmen
        if (Input.touchCount > 0)
        {
            //beskriver status för fingret som rör skärmen
            Touch touch = Input.GetTouch(0);
            //vart skärmen rördes
            Vector2 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
            if (Vector2.Distance(touchPosition, transform.position) < 1.0f || Vector2.Distance(touchPosition, transform.position) > 5.0f || touchPosition.x < transform.position.x)
            {
                attackIcon.SetActive(false);
                return;
            }


            //om skärmen har rörts
            if (touch.phase == TouchPhase.Began)
            {
                

                    canAttack = true;
                    attackIcon.SetActive(true);

            }
            //om man drar längs skärmen
            if (touch.phase == TouchPhase.Moved)
            {
                //om du kan attackera
                if (canAttack)
                {
                    //dra attack ikonen längs skärmen

                    //rb.AddForce(new Vector2(touchPosition.x, rb.transform.position.y));
                    attackIcon.transform.position = new Vector2(touchPosition.x, touchPosition.y);
                }
            }
            //om man slutar röra skärmen
            if (touch.phase == TouchPhase.Ended)
            {
                Debug.Log("Ended");
                //öka tapcount
                attackIcon.SetActive(false);

            }
        }
    }
}
