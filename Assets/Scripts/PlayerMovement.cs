﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float minX;
    [SerializeField]
    private float MaxX;
    [SerializeField]
    private float minY;
    [SerializeField]
    private float maxY;

    [SerializeField]
    private float speed;
    [SerializeField]
    private float jumpForce;

    Vector2 targetPosition;

    private Rigidbody2D rb;
    private Animator anim;
    private SpriteRenderer spriteRend;

    private bool flippedRight = true;
    private bool canJump = false;

    // Start is called before the first frame update
    void Start()
    {
        targetPosition = GetRandomPosition();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        spriteRend = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if ((Vector2)transform.position != targetPosition)
        {
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        }
        else
        {
            targetPosition = GetRandomPosition();
        }*/
        //Run();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            canJump = true;
        }
    }
    private void FixedUpdate()
    {

        Run();

        Jump();
    }
    Vector2 GetRandomPosition()
    {
        float randomY = Random.Range(minY, maxY);
        float randomX = Random.Range(minX, MaxX);
        return new Vector2(randomX, randomY);
    }
    void Run()
    {
        Vector3 spriteScale = transform.localScale;
        float x = Input.GetAxis("Horizontal");
        Vector3 moveDir = new Vector3(x, transform.localPosition.y, transform.localPosition.z);
        //rb.velocity = moveDir * speed;
        transform.position += moveDir * speed;
        Debug.Log(rb.velocity);
        if (x != 0)
        {
            anim.SetBool("isRunning", true);
        }
        else
        {
            anim.SetBool("isRunning", false);
        }
        //om man trycker vänster
        if (x < 0)
        {
            //om spelaren är vänd mot höger
            if (flippedRight)
            {
                flippedRight = false;
                //spriteScale.x = -8;
                transform.localScale = new Vector3(-8, transform.localScale.y, transform.localScale.z);
                Debug.Log(flippedRight);
                Debug.Log(x);
            }
        }
        if (x > 0)
        {
            if (!flippedRight)
            {
                //spriteScale.x = 8;
                transform.localScale = new Vector3(8, transform.localScale.y, transform.localScale.z);
                flippedRight = true;
                Debug.Log(flippedRight);
                Debug.Log(x);
            }
        }
    }
    void Jump()
    {
        if (canJump)
        {
            //rb.velocity = jumpVector * jumpForce;
            rb.AddForce(Vector2.up * jumpForce);
            canJump = false;
        }
    }
}
